from django import forms

# ფორმის აგება კოდის დონეზე
class TestForm(forms.Form): # აქ პარამეტრად გადავეცით ჯანგოს ფორმები.
    username = forms.CharField(label='Username', widget=forms.TextInput(attrs={'placeholder':'Username'}))
    password = forms.CharField(widget=forms.PasswordInput(attrs={'placeholder':'Password'}))

    # ვალიდაციები
    def clean(self, *args, **kwargs):
        username = self.cleaned_data.get('username')
        password = self.cleaned_data.get('password')

        if not username: # <- თუ 'username' არ არის, მაშინ...
            raise forms.ValidationError('Username is required')
        if not password: # <- თუ 'password' არ არის, მაშინ...
            raise forms.ValidationError('Password is required')

        # 'super()' ფუნქცია აბრუნებს ობიექტს, რომელიც წარმოადგენს მშობელ კლასს.
        return super(TestForm, self).clean(*args, **kwargs)
