# აქ ვწერთ ჩვენს მარშუტებს.
from django.urls import path
from . import views

urlpatterns = [
    path('', views.index, name='index'), # მივუთითეთ 'views'. ანუ საიტზე როცა შევა გაუშვებს ამ მეთოდს და დავარქვით სახელიც.
    path('hello/', views.hello, name='hello'), # ჩავამატეთ კიდევ ერთი მარშუტი.
]
