from django.shortcuts import render
from django.http import HttpResponse
from .forms import TestForm

# '.html' ფაილებში დაწერილი შიგთავსი რომ გამოვიდეს ბრაუზერში, მაგისთვის ეგ ყველაფერი უნდა მივაბათ ფუნქციას.
def index(request):
    form = TestForm(request.POST or None) # შემოვიტანეთ/ჩავწერეთ ფორმა და გადავეცით 'request' და ეს 'request'-ი თუ მოიცავს 'POST'-ს ან არაფერს.

    # აქ '.html' ფაილს გადავეცით ზემოთ დაწერილი მნიშვნელობები რადგან ეს მნიშვნელობები გამოვიტანოთ ჩვენს '.html' ფაილში.
    context = {
        'form': form,  # <- პირველ რიგში უნდა გადავცეთ ფორმა.
    }

    if form.is_valid(): # <- თუ ვალიდურია... (შეცდომის შემთხვევაში აღარ იქნება ვალიდური)
        # თუ ყველაფერი კარგადაა - ყველა მონაცემი შევსებულია რაც საჭირო იყო...
        username = form.cleaned_data.get('username') # გამოგვაქვს 'username'-ს მნიშვნელობა.
        password = form.cleaned_data.get('password') # გამოგვაქვს 'password'-ის მნიშვნელობა.

        # აქ '.html' ფაილს გადავეცით ზემოთ დაწერილი მნიშვნელობები რადგან ეს მნიშვნელობები გამოვიტანოთ ჩვენს '.html' ფაილში.
        context = {
            'form': form, # <- პირველ რიგში უნდა გადავცეთ ფორმა.
            'username': username,
            'password': password
        }
    return render(request, 'TechStoreApp/index.html', context) # გადავეცით 'request', ანუ რასაც ითხოვს და შემდეგ '.html' ფაილის მისამართი

def hello(request):
    return HttpResponse("Hello World!")

